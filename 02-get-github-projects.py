#!/usr/bin/env python3

# Find all active github repos for the wikimedia account

import re
import requests
import sqlite3
import sys
import pprint
import pdb

from datetime import datetime,timezone

pp = pprint.PrettyPrinter(indent=4)

pattern = re.compile(r'<(.*)>')

def active_fork(repo):
    """
    TODO: forks are hard...
    """
    r = requests.get(repo['url'], auth=())
    r.raise_for_status()
    full_repo = r.json()

    # Parent repo is archived
    parent_inactive = full_repo['parent']['archived'] == True

    # Fork is more up-to-date than the parent
    fork_updated = full_repo['updated_at']
    parent_updated = full_repo['parent']['updated_at']

    fork_more_active = fork_updated > parent_updated

    return parent_inactive or fork_more_active


def valid_repo(repo):
    """
    Filter out repos that are archived or forks.
    """
    if repo['archived']:
        return False

    # if repo['fork']:
    #     return active_fork(repo)

    # It's not archived, it's not a fork, it's a valid repo
    return True


conn = sqlite3.connect('projects.db')
c = conn.cursor()

existing_url_result = c.execute("SELECT url FROM urls").fetchall()
existing_urls = {existing[0] for existing in existing_url_result}

url = 'https://api.github.com/users/wikimedia/repos'
repos = []

while url:
    r = requests.get(url, auth=())
    r.raise_for_status()

    for project in r.json():
        if not valid_repo(project):
            print('Invalid:', project['full_name'])
            continue

        # Skip anything we've already found an existing mirror for
        if project['html_url'] in existing_urls:
            print('Already known:', project['html_url'])
            continue

        c.execute('INSERT INTO projects (name) VALUES (?)', (project['full_name'],))
        project_id = c.lastrowid

        c.execute(
            'INSERT INTO urls (system, url, project_id, active) VALUES (?, ?, ?, ?)',
            ('github', project['html_url'], project_id, 1)
            )

    conn.commit()

    links = r.headers['link'].split(',')
    for link in links:
        if 'next' not in link:
            continue
        uri, _ = link.split(';')

    try:
        new_url = pattern.search(uri).group(1).strip()
        if  new_url == url:
            sys.exit(0)
        url = new_url
    except AttributeError:
        url = None
        pass
    print('next: {}'.format(url))
