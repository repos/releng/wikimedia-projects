wikimedia-projects
==================

A work-in-progress attempt to list all Wikimedia projects across:

  - Wikimedia's Gerrit
  - Wikimedia's GitLab
  - Wikimedia's Phabricator/Phorge instance
  - The wikimedia organization on GitHub

...and possibly other places.

Background:

  - https://phabricator.wikimedia.org/T330347
  - https://phabricator.wikimedia.org/T347577
  - https://etherpad.wikimedia.org/p/canonical-repos

Based on: https://github.com/thcipriani/wikimedia-github-projects/
