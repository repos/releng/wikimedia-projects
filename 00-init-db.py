#!/usr/bin/env python3

# Create a projects.db

import os
import sqlite3

conn = sqlite3.connect('projects.db')
c = conn.cursor()

c.execute(
    '''CREATE TABLE projects (
         id INTEGER PRIMARY KEY,
         name TEXT,
         canonical_url_id INTEGER,
         FOREIGN KEY (canonical_url_id) REFERENCES urls(id)
       );
    '''
)

# A url:
#   - is for a particular system
#   - has a url string
#   - has an associated project
#   - is or is not active
c.execute(
    '''CREATE TABLE urls (
         id INTEGER PRIMARY KEY,
         system TEXT,
         url TEXT UNIQUE,
         project_id INTEGER,
         active INTEGER,
         FOREIGN KEY (project_id) REFERENCES projects(id)
       );
    '''
)

conn.commit()
conn.close()
