.PHONY: clean
DATE=$(shell date -I --utc)

projects.db: *.py
	python3 00-init-db.py
	python3 01-get-gerrit-projects.py
	python3 02-get-github-projects.py
	python3 03-get-gitlab-projects.py

clean:
	rm projects.db
