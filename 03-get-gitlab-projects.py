#!/usr/bin/env python3

import getpass
import gitlab
import json
import os
import pdb
import pprint
import re
import requests
import sqlite3
import sys
import re

from wikimedia_projects.util import get_system

GITLAB_URL = 'https://gitlab.wikimedia.org/'

def get_token():
    if os.environ.get('GITLAB_TOKEN'):
        return os.environ.get('GITLAB_TOKEN')
    return getpass.getpass('GitLab token: ').strip()

def get_gitlab():
    """Get a GitLab instance."""
    token = get_token()
    server = gitlab.Gitlab(GITLAB_URL, private_token=token)
    return server

def list_projects():
    """Get all projects on the server as a generator."""

    # https://gitlab.wikimedia.org/repos/
    # TODO: expand this to cover toolforge-repos, cloudvps-repos
    repos = server.groups.get(186)
    return repos.projects.list(all=True, as_list=False, include_subgroups=True)

def gimme_repl():
    import code
    import readline
    import rlcompleter
    vars = globals()
    vars.update(locals())
    readline.set_completer(rlcompleter.Completer(vars).complete)
    readline.parse_and_bind("tab: complete")
    code.InteractiveConsole(vars).interact()

pp = pprint.PrettyPrinter(indent=4)
server = get_gitlab()

conn = sqlite3.connect('projects.db')
c = conn.cursor()

existing_url_result = c.execute("SELECT url FROM urls").fetchall()
existing_urls = {existing[0] for existing in existing_url_result}

for project in list_projects():
    project_path = project.path_with_namespace
    gitlab_project_url = GITLAB_URL + project_path
    print(gitlab_project_url)

    # Skip anything we've already found an existing mirror for.
    # This isn't quite right...
    if gitlab_project_url in existing_urls:
        print('Already known:', gitlab_project_url)
        continue

    c.execute('INSERT INTO projects (name) VALUES (?)', (project_path,))
    project_id = c.lastrowid

    # This needs to be correct about system.  It also needs to be correct
    # about direction of relationship...
    project_urls = [gitlab_project_url]
    full_project = server.projects.get(project.id, lazy=True)
    mirrors = full_project.remote_mirrors.list()
    for mirror in mirrors:
        if mirror.url in existing_urls:
            print('Mirror already known:', mirror.url)
            continue
        project_urls.append(mirror.url)

    for project_url in project_urls:
        system = get_system(project_url)
        c.execute(
            'INSERT INTO urls (system, url, project_id, active) VALUES (?, ?, ?, ?)',
            (system, project_url, project_id, 1)
            )

    conn.commit()
