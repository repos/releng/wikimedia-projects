import re

def get_system(url):
   m = re.search('https?://(.*?)/', url)
   if m:
       return m.group(1)
   else:
       return 'unknown'
