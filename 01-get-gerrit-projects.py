#!/usr/bin/env python3

import json
import os
import pdb
import re
import requests
import sqlite3

GITHUB_URL = 'https://github.com/'

def trim_beg(haystack, needle):
    return haystack[len(needle):]

def gerrit_to_github(reponame):
    """
    Convert gerrit name to github name

    e.g., /mediawiki/core -> wikimedia/mediawiki-core
    """
    return os.path.join('wikimedia', reponame.strip().replace('/', '-'))

def canonical(reponame):
    """
    Make HEAD request to github

    If the status is 200, return the repo name.  If it's 301, return the
    canonical redirect.  Otherwise, assume failure and return None.
    """
    github_repo = GITHUB_URL + reponame
    r = requests.head(github_repo)
    print('checking "{}" - {}'.format(github_repo, r.status_code))

    if r.status_code == 200:
        return github_repo
    elif r.status_code == 301:
        return r.headers['location']

    print(r.status_code)
    return None

conn = sqlite3.connect('projects.db')
c = conn.cursor()

URL = 'https://gerrit.wikimedia.org/r/projects/?type=CODE'
r = requests.get(URL, auth=())
r.raise_for_status()

# # Trim leading ")]}'" garbage that Gerrit inserts to prevent XSSI:
# # https://gerrit-review.googlesource.com/Documentation/rest-api.html#output
projects = json.loads(r.text[5:])
projects = {k:v for k,v in projects.items() if v['state'] == 'ACTIVE'}

# Insert a project and a URL for every Gerrit repo
count = 0
for project, details in projects.items():
    count += 1
    all_urls = [link['url'] for link in details['web_links']]
    print(project)
    c.execute("INSERT INTO projects (name) VALUES (?)", (project,))
    project_id = c.lastrowid

    for url in all_urls:
        c.execute(
            "INSERT INTO urls (system, url, project_id, active) VALUES (?, ?, ?, ?)",
            ("gerrit", url, project_id, True)
            )
    if count % 10 == 0:
        conn.commit()

# Insert URLs for GitHub mirrors of Gerrit projects, checking to see if 
# they've been redirected elsewhere.
gerrit_projects = c.execute("SELECT id, name FROM projects").fetchall()
gerrit_projects_by_name = {name: pid for pid, name in gerrit_projects}

for gerrit_project_name, pid in gerrit_projects_by_name.items():
    github_url = canonical(gerrit_to_github(gerrit_project_name))
    if github_url is not None:
        print(f'{gerrit_project_name} ({pid}) -> {github_url}')
        c.execute(
            "INSERT INTO urls (system, url, project_id, active) VALUES (?, ?, ?, ?)",
            ("github", github_url, pid, True)
            )
    conn.commit()
